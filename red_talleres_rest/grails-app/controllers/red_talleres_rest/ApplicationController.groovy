package red_talleres_rest

import groovy.transform.CompileStatic
import grails.core.GrailsApplication
import grails.plugins.*

@CompileStatic
class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager

    def index() {
        [grailsApplication: grailsApplication, pluginManager: pluginManager]
    }
}
