package red_talleres_rest

import groovy.transform.CompileStatic
import grails.rest.*
import grails.converters.*

@CompileStatic
class ReplacementCategoryController extends RestfulController {
    static responseFormats = ['json', 'xml']
    ReplacementCategoryController() {
        super(ReplacementCategory)
    }
}
