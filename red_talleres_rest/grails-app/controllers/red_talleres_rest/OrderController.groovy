package red_talleres_rest

import groovy.transform.CompileStatic
import grails.rest.*
import grails.converters.*

@CompileStatic
class OrderController extends RestfulController {
    static responseFormats = ['json', 'xml']
    OrderController() {
        super(Order)
    }
}
