package red_talleres_rest

import groovy.transform.CompileStatic
import grails.rest.*
import grails.converters.*

@CompileStatic
class VehicleController extends RestfulController {
    static responseFormats = ['json', 'xml']
    VehicleController() {
        super(Vehicle)
    }
}
