package red_talleres_rest

import groovy.transform.CompileStatic
import grails.rest.*
import grails.converters.*

@CompileStatic
class FixKindController extends RestfulController {
    static responseFormats = ['json', 'xml']
    FixKindController() {
        super(FixKind)
    }
}
