package red_talleres_rest

import groovy.transform.CompileStatic
import grails.rest.*
import grails.converters.*

@CompileStatic
class FixController extends RestfulController {
    static responseFormats = ['json', 'xml']
    FixController() {
        super(Fix)
    }
}
