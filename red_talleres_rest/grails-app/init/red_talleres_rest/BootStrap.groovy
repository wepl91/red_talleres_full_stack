package red_talleres_rest

import grails.util.Environment
import red_talleres_rest.*

class BootStrap {

    def springSecurityService

    def init = { servletContext ->
      if (Environment.current != Environment.PRODUCTION) {
                
        FixKind smallFix = new FixKind(description: "Spark plug replacement").save(flush: true)

        Replacement sparkPlug = new Replacement(name: "Bujía", brand: "Motorcraft", url_image: "url_test", description: "Bujía simple").save(flush:true)
        ReplacementCategory someCategory = new ReplacementCategory(name: "Some category", description:"This category has no description")
        someCategory.addToReplacements(sparkPlug).save(flush:true)

        Fix sparkPlugReplacement = new Fix()
        sparkPlugReplacement.addToReplacements(sparkPlug).save(flush: true)

        smallFix.addToFixes(sparkPlugReplacement).save(flush: true)
        
        Vehicle oldCar = new Vehicle(plate: "ABC123",brand: "Toyota",model: "Corolla",year: 1995).save(flush:true)
        Order order = new Order(vehicle: oldCar, fix: sparkPlugReplacement)



        //USERS
        Authority adminRole = new Authority(authority: "ROLE_ADMIN").save(flush:true)
        User adminUser = new User(username: "admin", password: springSecurityService.encodePassword("admin")).save(flush:true)
        UserAuthority.create(adminUser, adminRole)
      
        Authority regularRole = new Authority(authority: "ROLE_USER").save(flush:true)
        User regularUser = new User(username: "user", password: springSecurityService.encodePassword("user")).save(flush:true)
        UserAuthority.create(regularUser, regularRole)
      }
    }
    def destroy = {
    }
}
