package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class FixKind {
  String description
  List fixes
   
    static hasMany = [fixes: Fix]
  
  static constraints = {
    description blank:false
  }
}
