package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class ReplacementCategory {
  
  String name
  String description

  static hasMany = [replacements : Replacement]
  
  static constraints = {
    name blank: false
    description blank: false
  }
}
