package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Fix {

  static hasMany = [replacements: Replacement]
  static hasOne = [order: Order]

  static constraints = {
  }
}
