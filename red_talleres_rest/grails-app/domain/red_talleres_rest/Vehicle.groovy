package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Vehicle {
  
  String plate
  String brand
  String model
  Integer year
  
  static hasOne = [order: Order]
  
  static constraints = {
    plate blank: false, unique: true
    brand blank: false
    model blank: false
    year min: 1980
  }
}
