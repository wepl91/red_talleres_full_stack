package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Replacement {

  Static belongsTo = [
    category: ReplacementCategory,
    fix: Fix]
  
  String name
  String brand
  String url_image
  String description
  
  
  static constraints = {
    name  blank: false
    brand blank: false
  }
}
