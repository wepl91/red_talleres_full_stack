package red_talleres_rest

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Order {

  Vehicle vehicle
  Fix fix

  static constraints = {
  }
}
